#IAM

```mermaid
graph BT;
    Contract(Contract);
    Person(Person);
    Organization(Organization);
    Identity(Identity);
    email(Email/ Username);
    PA(Personal Account);
    NPA(non-Personal Account);
    Attributes(Identity Attributes);
    Role(Role/ Groups);
    Context(Context as Attribute);



        subgraph HR
        Contract -- is signed by --> Person;
        Contract -- is signed by --> Organization;
        end

        subgraph Account
        Identity -- is defined by  --> Contract;
        email -- is owned by --> Identity  ;
        PA -- is identified by--> email;
        NPA
        end
        
        

        PA -- is owner off--> NPA;
        PA -- is identified by --> Attributes;
        NPA -- is identified by --> Attributes;
        
        
        
        PA --> Role;
        NPA --> Role;
        Context --> Role ;

    Process[Process];
    Information[Information];
    Application[Application];
    Infrastructure[Infrastructure];

    PA --> Process;
    PA --> Information;
    PA --> Application;
    
    subgraph Org;
    Process --> Information;
    Information --> Application;
    Application --> Infrastructure;
    end

    NPA --> Infrastrcture;
     
```


## Mermaid online resources
1. https://mermaid.live/edit
1. https://mermaid-js.github.io/mermaid/#/
1. https://jojozhuang.github.io/tutorial/mermaid-cheat-sheet/
1. https://about.gitlab.com/handbook/tools-and-tips/mermaid/
